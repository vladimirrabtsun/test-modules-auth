import axios, {AxiosError, AxiosInstance, AxiosPromise, AxiosRequestConfig} from "axios";
import DateUtils from "../utils/DateUtils";

export enum HttpStatusCodes {
    Created = 201,
    Unauthorized = 401
}

export interface ApiRoutes {
    [key: string]: {
        _http: AxiosInstance | undefined;
        setHttp: (http: AxiosInstance) => void;
    };
}

export default class Api {
    public auth: Authorization;

    public http: AxiosInstance;

    public routes: ApiRoutes = {};

    constructor(
        private baseUrl: string,
        private name: string,
        private key: string,
        routes: any
    ) {
        this.auth = new Authorization(this.baseUrl, this.name, this.key);

        this.http = axios.create();

        this.http.interceptors.request.use(async (config: AxiosRequestConfig) => {
            if (this.auth.getNewTokenPromise) {
                await this.auth.getNewTokenPromise;
            }

            if (!this.auth.token) {
                await this.auth.getNewToken();
            }
            config.headers.Authorization = this.auth.token;
            config.baseURL = this.baseUrl;
            return config;
        });

        this.http.interceptors.response.use(undefined, async (error: AxiosError) => {
            if (error.response && error.response.status === HttpStatusCodes.Unauthorized) {
                if (this.auth.getNewTokenPromise) {
                    await this.auth.getNewTokenPromise;
                } else {
                    await this.auth.getNewToken();
                }
                return this.http(error.config);
            }
            return error;
        });

        Object.keys(routes).forEach(key => {
            this.routes[key] = new routes[key];
            this.routes[key].setHttp(this.http);
        });
    }
}

export interface GetNewTokenResponse {
    token: string;
    expires_in: number;
    client_id: string;
}

export class Authorization {

    private _token: string | undefined;
    private _expiredAt: Date | undefined;
    private _clientId: string | undefined;

    private readonly _data: { name: string, key: string };

    private readonly _baseStorageItemPrefix = 'api.platform:auth';
    private readonly _tokenStorageItem = `${this._baseStorageItemPrefix}:token`;
    private readonly _expiredAtStorageItem = `${this._baseStorageItemPrefix}:expired-at`;
    private readonly _clientIdStorageItem = `${this._baseStorageItemPrefix}:client-id`;

    public getNewTokenPromise: AxiosPromise<GetNewTokenResponse> | undefined;

    constructor(
        private baseUrl: string,
        private name: string,
        private key: string
    ) {
        this._data = {
            name: this.name,
            key: this.key
        };
    }

    // @ts-ignore
    public async getNewToken(): Promise<void> {
        this.getNewTokenPromise = axios.post<GetNewTokenResponse>(`${this.baseUrl}/auth/consumers/token`, this._data);
        const response = await this.getNewTokenPromise;
        this.getNewTokenPromise = undefined;
        this._setToken(response.data);
    }

    public get token(): string | undefined {
        if (!this._token || this._expiredAt || this._clientId) {
            const storedToken = localStorage.getItem(this._tokenStorageItem);
            const storedExpiredAt = localStorage.getItem(this._tokenStorageItem);
            const storedClientId = localStorage.getItem(this._tokenStorageItem);

            if (storedToken && storedExpiredAt && storedClientId) {
                this._token = storedToken;
                this._expiredAt = new Date(storedExpiredAt);
                this._clientId = storedClientId;
            }
        }

        if (this._expiredAt === undefined || this._expiredAt >= new Date()) {
            this._clearToken();
        }

        return this._token;
    }

    private _setToken({ token, expires_in, client_id }: GetNewTokenResponse): void {
        this._token = `Bearer ${token}`;
        localStorage.setItem(this._tokenStorageItem, this._token);

        this._expiredAt = DateUtils.nowWithMilliseconds(expires_in);
        localStorage.setItem(this._expiredAtStorageItem, this._expiredAt.toString());

        this._clientId = client_id;
        localStorage.setItem(this._clientIdStorageItem, this._clientId);
    }

    private _clearToken(): void {
        this._token = undefined;
        localStorage.removeItem(this._tokenStorageItem);

        this._expiredAt = undefined;
        localStorage.removeItem(this._expiredAtStorageItem);

        this._clientId = undefined;
        localStorage.removeItem(this._clientIdStorageItem);
    }

}
