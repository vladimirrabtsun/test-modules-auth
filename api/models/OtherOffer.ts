export interface OtherOffer {
    source_name: string;
    amount: number;
}
