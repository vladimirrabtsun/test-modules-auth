import {Services} from "./Services";
import {Cabin} from "./Cabin";
import {BaggageOption} from "./BaggageOption";

export interface Segment {
    origin: string;
    airport_dest: string;
    arr_time_iso: string;
    dep_terminal: string;
    dep_time_iso: string;
    carrier_name: string;
    stop_locations: string[];
    dest_code: string;
    airport_dest_terminal: string;
    equipment: string;
    flight_num: string;
    stops: number;
    airport_origin: string;
    cabin: Cabin;
    dep_time: string;
    dest: string;
    origin_code: string;
    baggage_options: BaggageOption[];
    arr_time: string;
    plane: string;
    services: Services;
    fare_basis_code: string;
    airport_origin_terminal: string;
    arr_terminal: string;
    carrier: string;
    fare_seats: number;
}
