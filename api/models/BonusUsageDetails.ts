export interface BonusUsageDetails {
    maximum_amount: number;
    minimum_amount: number;
    minimum_percentage: number;
    maximum_percentage: number;
}
