export interface BonusAccrualDetails {
    percentage: number;
    amount: number;
}
