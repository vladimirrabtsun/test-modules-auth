export interface Services {
    '1PC': {
        full_description: string;
        alt_text: string;
        code: string;
        description: string;
        title: string;
        default: string;
        solution: string;
        value: string;
        icon: string;
    };
}
