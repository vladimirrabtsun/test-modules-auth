export interface Agent {
    link: string;
    type: string;
    image_url: string;
    name: string;
}
