export enum FlightTypes {
    OneWay = 'oneway',
    RoundTrip = 'roundtrip',
    MultiCity = 'multicity',
}
