export interface GroupDescription {
    type_text: string;
    name: string;
    payment_type: string;
}
