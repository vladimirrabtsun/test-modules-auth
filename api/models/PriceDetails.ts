export interface PriceDetails {
    base_raw: number;
    modifiers: string;
    modifiers_raw: number;
    taxes: string;
    base: string;
    taxes_raw: number;
}
