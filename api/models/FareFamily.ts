import {GroupDescription} from "./GroupDescription";

export interface FareFamily {
    package_id: number;
    currency: string;
    code: string;
    name: string;
    total_price: number;
    extra_fare: number;
    selected: boolean;
    grouped_descriptions: Partial<{
        'INCLUDE': GroupDescription[];
        'CHARGABLE': GroupDescription[];
        'NOT OFFER': GroupDescription[];
    }>;
}
