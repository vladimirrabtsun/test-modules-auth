import {Segment} from "./Segment";
import {Agent} from "./Agent";

export interface Itinerary {
    dep_date: string;
    hash: string;
    dir_index: number;
    price: {
        currency: string;
        amount: string;
        price_raw: number;
    };
    layovers: number[];
    agent: Agent | null;
    arr_date: string;
    allowed_offers: string[];
    carrier_name: string;
    is_meta: boolean;
    segments: Segment[];
    stops: number;
    carrier: string;
    refundable: boolean;
    traveltime: number;
}
