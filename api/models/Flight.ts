import {FareFamily} from "./FareFamily";
import {BonusUsageDetails} from "./BonusUsageDetails";
import {BonusAccrualDetails} from "./BonusAccrualDetails";
import {Services} from "./Services";
import {PriceDetails} from "./PriceDetails";
import {Itinerary} from "./Itinerary";

export interface Flight {
    itineraries: Itinerary[];
    price_details: PriceDetails;
    price: string;
    currency: string;
    bonus_usage: boolean;
    services: Services;
    price_raw: number;
    validating_carrier: string;
    id: string;
    has_meta: boolean;
    has_offers: boolean;
    group_hash: string;
    best_time: number;
    bonus_accrual: boolean;
    bonus_accrual_details: BonusAccrualDetails | null;
    bonus_usage_details: BonusUsageDetails | null;
    fare_families?: FareFamily | null;
    is_lowcost?: boolean;
    provider: string;
    refundable: boolean;
    provider_class: string;
}
