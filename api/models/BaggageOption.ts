export interface BaggageOption {
    value: number;
    unit: string;
}
