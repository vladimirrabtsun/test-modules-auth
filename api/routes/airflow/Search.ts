import {AxiosResponse} from 'axios';
import DateUtils from "../../../utils/DateUtils";
import BaseRoute from "../BaseRoute";

interface Itinerary {
    airport: {
        dep: string;
        arr: string;
    }
    depAt: Date;
}

interface Passengers {
    adults: number;
    children: number;
    infants: number;
    youths: number;
}

enum CabinClasses {
    Economy = 'E',
    Business = 'B'
}

export interface SearchPayload {
    itineraries: Itinerary[];
    passengers: Passengers;
    cabinClass: CabinClasses;
}

export class Search extends BaseRoute {

    private _data: string | undefined;

    public setData(payload: SearchPayload): Search {
        const itineraries: string = payload.itineraries
            .map(i => `${i.airport.arr}-${i.airport.dep}${DateUtils.dateToStr(i.depAt, '', true)}`)
            .join();

        const { adults, children, infants, youths } = payload.passengers;
        const passengers: string = `${adults}${children}${infants}${youths}`;

        this._data = `${itineraries}${passengers}${payload.cabinClass}`;
        return this;
    }

    public fetch(): Promise<AxiosResponse<ApiSearchResponse>> {
        if (!this._http) {
            throw new Error('http not defined');
        }

        if (!this._data) {
            throw new Error('data not defined');
        }

        return this._http.post<ApiSearchResponse>('/airflow/search', this._data);
    }
}

export interface ApiSearchResponse {
    search_id: string;
}
