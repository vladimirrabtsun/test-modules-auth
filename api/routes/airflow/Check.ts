import BaseRoute from "../BaseRoute";
import {AxiosResponse} from "axios";

export interface CheckPayload {
    search_id: string;
}

export interface CheckResponse {

}

export default class Check extends BaseRoute {
    private _data: CheckPayload | undefined;

    public setData(payload: CheckPayload): Check {
        this._data = payload;
        return this;
    }

    public fetch(): Promise<AxiosResponse<CheckResponse>> {
        if (!this._http) {
            throw new Error('http not defined');
        }

        if (!this._data) {
            throw new Error('data not defined');
        }

        return this._http.get<CheckResponse>(`/airflow/search/results/${this._data.search_id}`);
    }
}
