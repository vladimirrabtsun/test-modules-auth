import {AxiosInstance} from "axios";

export default abstract class BaseRoute {
    protected _http: AxiosInstance | undefined;

    protected setHttp(http: AxiosInstance): void {
        this._http = http;
    }
}
