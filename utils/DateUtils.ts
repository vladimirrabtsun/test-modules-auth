import addMilliseconds from 'date-fns/addMilliseconds';

export default class DateUtils {

    /**
     * Gets the date as a string, for example '2020-12-31'.
     * @param date - Date object.
     * @param delimiter - String character to split the return value.
     * @param reverse - If need to change the order of the day, month, year.
     */
    static dateToStr(date: Date, delimiter: string = '', reverse: boolean = false): string {
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDate();
        if (reverse) {
            return `${year}${delimiter}${month}${delimiter}${day}`;
        }
        return `${day}${delimiter}${month}${delimiter}${year}`;
    }

    static nowWithMilliseconds(milliseconds: number): Date {
        return addMilliseconds(new Date(), milliseconds);
    }
}
